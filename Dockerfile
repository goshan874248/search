FROM node:current-slim

WORKDIR /usr/src/search

COPY package.json /usr/src/search

RUN yarn install

EXPOSE 8080

CMD [ "yarn", "start" ]

COPY . /usr/src/search