import React from 'react';
import ReactDOM from 'react-dom';
import { Main } from './components/Main';

import './globalStyles.css';

ReactDOM.render(<Main />, document.getElementById('app'));
