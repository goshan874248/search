import React from 'react';
import { shallow } from 'enzyme';
import { Main } from './Main';

import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

test('CheckboxWithLabel changes the text after click', () => {
  // Render a checkbox with label in the document
  const checkbox = shallow(<Main />);

  expect(checkbox.text()).toEqual('hhh<MenuAppBar />');
});