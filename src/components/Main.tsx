import React, { FC } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import { styled } from 'linaria/react';
import { MenuAppBar } from './MenuBar';

interface MainProps {
  start?: boolean;
}

const Main: FC<MainProps> = () => {
  return (
    <Wrapper>
      <MenuAppBar />
    </Wrapper>
  );
};

export { Main };

const Wrapper = styled.div`
  height: 100%;
`
